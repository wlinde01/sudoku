﻿namespace Sudoku

type Cell(X:int,Y:int,cell: Set<int>) = 
    member this.X = X
    member this.Y = Y
    member this.cell = cell 


module Solver =

    // Has a direct test, hence leaving it in outer scope
    let inSameBox ((xToCheck,yToCheck) , (xToCheckAgainst, yToCheckAgainst )):bool =
        let inScope (i, j) = i/3 = j/3
        (inScope (xToCheck, xToCheckAgainst) && inScope (yToCheck, yToCheckAgainst))

    // Cell to check, is in row, col or box of checkee
    let inScopeCell (x,y,colNumber, rowNumber) = 
        let sameCel((x,y),(a,b)) = (x=a && y=b)
        (inSameBox((x,y),(colNumber, rowNumber)) || (x = colNumber) || (y = rowNumber)) && not (sameCel((x,y),(colNumber,rowNumber)))

    // Transforms one puzzle row, into the specification list of sets of ints
    let transformRow (inputList : List<int>) =
        let rec loop list =
            match list with 
                | h::t when (h = 0) -> (Set([1..9]))::loop(t)
                | h::t -> Set([h])::loop(t)
                | [] -> []
        loop inputList

    // The Transform function, as per specification
    let transform (puzzle : List<List<int>>) = 
        puzzle
        |> List.map transformRow


    let findSingletons (transMatrix : List<List<Set<int>>>) =
        let singletonPredicate(set:Set<'t>) = set.Count = 1
        transMatrix
        |> List.mapi (fun y row -> 
            row
            |> List.mapi (fun x cell -> 
                            if (singletonPredicate(cell)) then
                                new Cell(x,y,cell)
                            else new Cell(0,0,Set([])))) // not pretty
            |> Seq.concat
            |> Seq.filter (fun item -> item.cell.Count > 0)
            |> List.ofSeq


    // Use singleton to clear them out from any "In Scope" sets.
    let purgeSets(x,y,singleton, matrix) : List<List<Set<int>>> =         

        matrix
        |> List.mapi (fun rowNumber row -> 
            row
            |> List.mapi (fun colNumber cell -> 
                            if (inScopeCell(x,y,colNumber, rowNumber)) then
                                cell-Set([singleton])
                            else cell
                            )
            )


    // Use singletons to clear them out from any "In Scope" sets.
    let purgeMatrix (transMatrix :  List<List<Set<int>>>) =
        let singletons = findSingletons (transMatrix)
        let rec loop (singletons: List<Cell>, matrix) = 
            match singletons with
            | h::t -> loop(t, purgeSets(h.X, h.Y, List.ofSeq(h.cell).[0], matrix))
            | [] -> matrix

        loop(singletons,transMatrix)


    // Returns a unified set of number possibilities off all puzzle sets in the same row of the puzzle matrix
    let inScopeRowSets (matrix:List<List<Set<int>>>, candidate:Cell) =

        let inScopeRow (x,y,colNumber, rowNumber) = 
            let sameCel((x,y),(a,b)) = (x=a && y=b)
            ((y = rowNumber)) && not (sameCel((x,y),(colNumber,rowNumber)))

        
        matrix.Item(candidate.Y) // Grab the relevant row
        |> List.mapi (fun colNumber cell -> 
                        if (inScopeRow(candidate.X, candidate.Y, colNumber, candidate.Y)) then
                            cell
                        else Set([]))
        |> List.reduce (+)

   

   // Returns a unified set of number possibilities off all puzzle sets in the same collumn of the puzzle matrix
    let inScopeColSets (matrix:List<List<Set<int>>>, candidate:Cell) =   
        let inScopeColl (x,y,colNumber, rowNumber) = 
            let sameCel((x,y),(a,b)) = (x=a && y=b)
            ((x = colNumber) || (y = rowNumber)) && not (sameCel((x,y),(colNumber,rowNumber)))

        matrix
        |> List.mapi (fun rowNumber row -> 
            row
            |> List.mapi (fun colNumber cell -> 
                            if (inScopeColl(candidate.X, candidate.Y, colNumber, rowNumber)) then
                                cell
                            else Set([])
                            )
            )
        |> List.map (fun row ->
            row
            |> List.reduce (+)
            )
        |> List.reduce (+)



    // Returns a unified set of number possibilities off all puzzle sets in the same " 3x3 box" of the puzzle matrix
    let inScopeBoxSets (matrix:List<List<Set<int>>>, candidate:Cell) =        
        let inScopeBox (x,y,colNumber, rowNumber) = 
            let sameCel((x,y),(a,b)) = (x=a && y=b)
            (inSameBox((x,y),(colNumber, rowNumber)) && not (sameCel((x,y),(colNumber,rowNumber))))

        matrix
        |> List.mapi (fun rowNumber row -> 
            row
            |> List.mapi (fun colNumber cell -> 
                            if (inScopeBox(candidate.X, candidate.Y, colNumber, rowNumber)) then
                                cell
                            else Set([])
                            )
            )
        |> List.map (fun row ->
            row
            |> List.reduce (+)
            )
        |> List.reduce (+)       
                
    // Find any Sets that can be reduced to Singletons
    let collapseSets(matrix:List<List<Set<int>>>) = 
        
        let collapse (candidate : Cell) : Set<int>= 
          let rowSubset = lazy (candidate.cell - inScopeRowSets (matrix, candidate))
          let colSubset = lazy (candidate.cell - inScopeColSets (matrix, candidate))
          let boxSubset = lazy (candidate.cell - inScopeBoxSets (matrix, candidate))

          match candidate with
          | _ when (rowSubset.Force().Count = 1) -> rowSubset.Force()
          | _ when (colSubset.Force().Count = 1) -> colSubset.Force()
          | _ when (boxSubset.Force().Count = 1) -> boxSubset.Force()
          | c -> c.cell
        
        matrix
        |> List.mapi (fun y row -> 
            row
            |> List.mapi(fun x cell -> collapse(new Cell(x,y,cell))))   
        
    // What it says on the tin:
    let purgeAndCollapse matrix = 
        matrix
        |> purgeMatrix
        |> collapseSets    

    // Is it Solved ?
    let solved (matrix:List<List<Set<int>>>) : bool =
        matrix
        |> List.mapi (fun y row -> 
            row
            |> List.mapi(fun x cell -> cell.Count = 1))   
        
        |> List.map (fun row ->
            row
            |> List.reduce (&&))
        |> List.reduce (&&)  
   



    // Main solve function
    let solve (puzzle : List<List<int>>) = 
        
        let rec tryToSolve (matrixN0) =
            match purgeAndCollapse matrixN0 with
            | matrixN1 when (solved(matrixN0) || matrixN1.Equals(matrixN0) ) -> matrixN0 // pull the plug when it's solved or after things stop changing 
            | matrixN1 -> tryToSolve (matrixN1)
        
        tryToSolve (transform puzzle)



