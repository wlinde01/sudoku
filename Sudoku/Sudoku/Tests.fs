﻿module TestsStepDefinitions

open Xunit
open Xunit.Should

//http://www.sudoku.ws/easy-6.htm
//http://www.sudoku.ws/easy-6-solution.htm
let puzzle = 
    [
        [0;0;0;0;9;0;0;0;4];
        [4;1;0;0;0;3;0;0;0];
        [8;0;7;6;0;4;2;1;0];
        [0;0;1;0;0;7;0;0;2];
        [0;6;0;0;4;0;0;9;0];
        [2;0;0;5;0;0;7;0;0];
        [0;4;8;3;0;6;9;0;7];
        [0;0;0;4;0;0;0;2;1];
        [6;0;0;0;1;0;0;0;0];
    ]

// Only used via 
let puzzle2 = 
    [[0;2;5;0;0;1;0;0;0];
    [1;0;4;2;5;0;0;0;0];
    [0;0;6;0;0;4;2;1;0];
    [0;5;0;0;0;0;3;2;0];
    [6;0;0;0;2;0;0;0;9];
    [0;8;7;0;0;0;0;6;0];
    [0;9;1;5;0;0;6;0;0];
    [0;0;0;0;7;8;1;0;3];
    [0;0;0;6;0;0;5;9;0]]

let solution = 
    [
        [5;2;6;1;9;8;3;7;4];
        [4;1;9;2;7;3;8;6;5];
        [8;3;7;6;5;4;2;1;9];
        [3;8;1;9;6;7;5;4;2];
        [7;6;5;8;4;2;1;9;3];
        [2;9;4;5;3;1;7;8;6];
        [1;4;8;3;2;6;9;5;7];
        [9;7;3;4;8;5;6;2;1];
        [6;5;2;7;1;9;4;3;8];
    ]

let getZeroSets n = [for i in 1..n do yield Set([1..9])]    
let expectedTransformedMatrix = 
    [
        List.concat [ getZeroSets(4); [Set([9])]; getZeroSets(3); [Set([4])]; ]   
        List.concat [ [Set([4])]; [Set([1])]; getZeroSets(3); [Set([3])]; getZeroSets(3); ]   
        List.concat [ [Set([8])]; getZeroSets(1); [Set([7])]; [Set([6])]; getZeroSets(1); [Set([4])]; [Set([2])]; [Set([1])]; getZeroSets(1); ]   
        List.concat [ getZeroSets(2); [Set([1])]; getZeroSets(2); [Set([7])]; getZeroSets(2); [Set([2])]; ]   
        List.concat [ getZeroSets(1); [Set([6])]; getZeroSets(2); [Set([4])]; getZeroSets(2); [Set([9])]; getZeroSets(1); ]               
        List.concat [ [Set([2])]; getZeroSets(2); [Set([5])]; getZeroSets(2); [Set([7])]; getZeroSets(2); ]               
        List.concat [ getZeroSets(1); [Set([4])]; [Set([8])]; [Set([3])]; getZeroSets(1); [Set([6])]; [Set([9])]; getZeroSets(1); [Set([7])];]               
        List.concat [ getZeroSets(3); [Set([4])]; getZeroSets(3); [Set([2])]; [Set([1])]; ]               
        List.concat [ [Set([6])]; getZeroSets(3); [Set([1])]; getZeroSets(4); ]               

    ]


[<Fact>] 
let TestSolver() = 
    Assert.Equal<Set<int> list list>(Sudoku.Solver.transform solution, Sudoku.Solver.solve puzzle)

[<Fact>] 
let TestTransformFunction () = 
    Assert.Equal<Set<int> list list>(expectedTransformedMatrix, Sudoku.Solver.transform puzzle)

[<Fact>] 
let TestTransformRowFunction () = 
    let expectedListOfSets = 
        [Set([1..9]);Set([1..9]);Set([1..9]);Set([1..9]);Set([9]);Set([1..9]);Set([1..9]);Set([1..9]);Set([4]);]            
    Assert.Equal<Set<int> list>(expectedListOfSets, Sudoku.Solver.transformRow puzzle.Head)
 
[<Fact>] 
let TestFindNextSingletonFunction () =
   let expected = new Sudoku.Cell(4,0,Set([9]))
   let actual =  Sudoku.Solver.findSingletons(expectedTransformedMatrix)
   Assert.Equal (expected.X , actual.[0].X)
   Assert.Equal (expected.Y , actual.[0].Y)
   Assert.Equal<Set<int>> (expected.cell , actual.[0].cell)

   let expected = new Sudoku.Cell(0,1,Set([4]))
   Assert.Equal (expected.X , actual.[2].X)
   Assert.Equal (expected.Y , actual.[2].Y)
   Assert.Equal<Set<int>> (expected.cell , actual.[2].cell)

   

[<Fact>] 
let TestInSameBoxFunction () =
    let xyToCheck = (2,2)
    let xyToCheckAgainst = (2,0)
    Assert.True(Sudoku.Solver.inSameBox (xyToCheckAgainst, xyToCheck))
    
    let xyToCheck = (3,1)
    let xyToCheckAgainst = (2,0)
    Assert.False(Sudoku.Solver.inSameBox (xyToCheckAgainst, xyToCheck))

    let xyToCheck = (1,1)
    let xyToCheckAgainst = (7,7)
    (Sudoku.Solver.inSameBox (xyToCheckAgainst, xyToCheck)).ShouldBeFalse

[<Fact>]
let TestPurgeSetsFunction () = 
    let touched = Set([1;2;3;4;5;6;7;8])
    let untouched = Set([1;2;3;4;5;6;7;8;9])
    let purgedMatrix = 
        Sudoku.Solver.purgeSets (4,0,9,expectedTransformedMatrix)
    let row1,rest = (purgedMatrix.Head, purgedMatrix.Tail)
    
    //same row
    Assert.Equal<Set<int>>(touched, row1.[0])
    Assert.Equal<Set<int>>(touched, row1.[7])
    //singleton set
    Assert.Equal<Set<int>>(Set([4]), rest.Head.[0])
    //same box
    Assert.Equal<Set<int>>(touched, rest.Head.[3])
    //not in scope
    Assert.Equal<Set<int>>(untouched, rest.Head.[6])
    Assert.Equal<Set<int>>(untouched, rest.Head.[2])
    //same collumn
    Assert.Equal<Set<int>>(touched, purgedMatrix.[7].[4])
    



